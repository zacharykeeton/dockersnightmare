# Docker's Nightmare

## Useful for running Nightmare.js in a CI/CD pipeline or for web scraping cron jobs.

A sample project and Dockerfile to get Nightmare.js to run headless in a Docker container. Meant to be a reference/starter to help you in your work.

In the example script, nightmare.js script goes to the DuckDuckGo homepage and grabs the page title.  The Dockerfile included the various UI libs and virtual frame buffer helpers that allow electron (nightmare's dependency) to run inside the headless container.

To see it in action, simply clone this repo, build the image, and run. Like so:

```sh
@zacharykeeton:~$ git clone https://gitlab.com/zacharykeeton/dockersnightmare.git
@zacharykeeton:~$ cd dockersnightmare/
@zacharykeeton:~/dockersnightmare$ docker build --tag nightmaretest . 
... [docker build output snipped] ...
@zacharykeeton:~/dockersnightmare$ docker run nightmaretest
DuckDuckGo — Privacy, simplified.
```