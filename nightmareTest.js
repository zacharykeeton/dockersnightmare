const Nightmare = require('nightmare');
 
const nightmare = Nightmare();
 
nightmare.goto('https://duckduckgo.com')
  .evaluate(() => {
    return document.title;
  })
  .end()
  .then((title) => {
    console.log(title);
  })