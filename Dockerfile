FROM node:10.15.3

WORKDIR /usr/src/app

COPY . .

RUN apt-get update && \
    apt-get -y install libxss1 libxtst6 libgconf-2-4 libnss3 libasound2 xvfb && \
    npm install -g xvfb-maybe && \
    npm install

# Run the command on container startup
CMD ["xvfb-maybe", "node", "nightmareTest.js"]